{
  self,
  pkgs,
}: let
  user = "admin";
in [
  self.nixosModules.nix-data-ops

  {
    nix-data-ops = {
      spark.worker.enable = true;
    };

    users.users.${user} = {
      isNormalUser = true;
      home = "/home/${user}";
      extraGroups = ["wheel"];
      initialPassword = "spark";
    };
    system.stateVersion = "23.11";
  }
]
