{
  self,
  pkgs,
}: let
  user = "admin";
in [
  self.nixosModules.nix-data-ops

  {
    nix-data-ops = {
      # Data lake
      minio = {
        enable = true;
        host = "0.0.0.0";
        port = 9000;
        port_ui = 9001;
        openFirewall = true;
        dataDir = ["/var/lib/minio/data"];
        region = "us-east-1";
        extraEnvironment = {};
        package = pkgs.minio;
        rootCredentialsFile = pkgs.writeTextFile {
          name = "minio-credentials";
          text = ''
            MINIO_ROOT_USER=minio
            MINIO_ROOT_PASSWORD=miniorootpwd
          '';
        };
      };

      # Distributed Computing
      spark = {
        master = {
          enable = true;
          port = 7077;
          port_ui = 7078;
          openFirewall = true;
        };

        worker = {
          enable = true;
          master = "0.0.0.0:7077";
          port = 7079;
          port_ui = 7080;
          max_memory = "24g";
          max_cores = "4";
          openFirewall = true;
        };
      };

      ballista = {
        enable = true;
        scheduler.enable = true;
        scheduler-ui = {
          openFirewall = true;
          enable = true;
        };
        executors = {
          ex1 = {
            enable = true;
            port = 50012;
          };
          ex2 = {
            enable = true;
            port = 50013;
          };
        };
      };

      # Orchestration
      airflow = {
        enable = true;
        dataDir = /tmp;
        port = 8080;
        openFirewall = true;
      };

      # Jupyterhub
      jupyterhub = {
        enable = true;
        port = 9833;
        host = "0.0.0.0";
        openFirewall = true;
      };

      # RStudio Server
      rstudio-server = {
        enable = true;
        host = "0.0.0.0";
        openFirewall = true;
        r-packages = with pkgs.rPackages; [
          dplyr
        ];
      };
    };

    console.keyMap = "de";

    programs.fish.enable = true;

    programs.ssh.startAgent = true;

    users.users.${user} = {
      isNormalUser = true;
      home = "/home/${user}";
      extraGroups = ["wheel"];
      initialPassword = "spark";
    };
    users.allowNoPasswordLogin = true;

    system.stateVersion = "23.11";
  }
]
