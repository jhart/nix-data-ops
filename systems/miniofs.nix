{
  self,
  pkgs,
}: let
  user = "admin";
in [
  self.nixosModules.nix-data-ops

  {
    nix-data-ops.minio = {
      enable = true;
    };

    console.keyMap = "de";

    programs.fish.enable = true;

    programs.ssh.startAgent = true;

    users.users.${user} = {
      isNormalUser = true;
      home = "/home/${user}";
      extraGroups = ["wheel"];
      initialPassword = "spark";
    };
    users.allowNoPasswordLogin = true;

    system.stateVersion = "23.11";
  }
]
