{
  self,
  pkgs,
}: let
  user = "admin";
  packages = with pkgs.rPackages; [
    EValue
    Hmisc
    arsenal
    cowplot
    crayon
    descr
    DescTools
    diagram
    dplyr
    extrafont
    finetune
    foreign
    fst
    furniture
    ggplot2
    ggthemes
    gt
    gtsummary
    haven
    here
    ipw
    janitor
    jsonlite
    knitr
    labelled
    languageserver
    lavaan
    ltm
    mice
    naniar
    pROC
    rlang
    rpart
    scales
    semTools
    sjPlot
    sjmisc
    skimr
    srvyr
    stargazer
    survey
    tidymodels
    tidytidbits
    tidyverse
    vctrs
    vip
    visdat
    weights
    xml2
    xts
  ];
in [
  self.nixosModules.nix-data-ops

  {
    nix-data-ops.rstudio-server = {
      enable = true;
      r-packages = packages;
      openFirewall = true;
    };

    console.keyMap = "de";

    programs.fish.enable = true;

    programs.ssh.startAgent = true;

    users.users.${user} = {
      isNormalUser = true;
      home = "/home/${user}";
      extraGroups = ["wheel"];
      initialPassword = "spark";
    };

    system.stateVersion = "23.11";
  }
]
