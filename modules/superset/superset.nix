# Module for Apache superset
# When enabled, it starts the Apache superset docker container using the official docker-compose.yaml file
{
  lib,
  pkgs,
  config,
}: let
  cfg = config.nix-data-ops.superset;
  env_path = toString ./docker/.env;
  docker_path = toString ./docker;
  dockerfile = builtins.toFile "superset-docker" (builtins.replaceStrings 
    [
      "8088:8088" 
      "env_file: docker/.env" 
      "./docker"
    ] 
    [
      "${toString cfg.port}:8088" 
      "env_file: ${env_path}" 
      "${docker_path}"
    ] 
    (lib.readFile ./docker-compose.yaml)
  );
in
  with lib; {
    options.nix-data-ops.superset = {
      enable = mkOption {
        default = false;
        description = "Enable Apache Superset";
      };

      port = mkOption {
        default = 8088;
        type = types.port;
        description = "Port of the web interface";
      };

      openFirewall = mkOption {
        default = false;
        type = types.bool;
        description = "Opens the firewall for Apache superset";
      };
    };
    config = mkIf cfg.enable {
      systemd.services.apache-superset = {
        wantedBy = ["multi-user.target"];
        after = ["docker.service" "docker.socket"];
        environment = {
          DATABASE_PORT="5432";
          DATABASE_DIALECT="postgresql";
          POSTGRES_DB="superset";
          POSTGRES_USER="superset";
          POSTGRES_PASSWORD="superset";

          # Add the mapped in /app/pythonpath_docker which allows devs to override stuff
          PYTHONPATH="/app/pythonpath:/app/docker/pythonpath_dev";
          REDIS_HOST="redis";
          REDIS_PORT="6379";

          FLASK_DEBUG="true";
          SUPERSET_ENV="development";
          SUPERSET_LOAD_EXAMPLES="yes";
          CYPRESS_CONFIG="false";
          SUPERSET_PORT="8088";
          MAPBOX_API_KEY="";

          # Make sure you set this to a unique secure random value on production
          SUPERSET_SECRET_KEY="TEST_NON_DEV_SECRET";

          ENABLE_PLAYWRIGHT="false";
          PUPPETEER_SKIP_CHROMIUM_DOWNLOAD="true";
          BUILD_SUPERSET_FRONTEND_IN_DOCKER="true";
        };

        serviceConfig = {
          Type = "simple";
          ExecStart = ''
            /bin/sh -c '${pkgs.docker-compose}/bin/docker-compose -f ${dockerfile} up'
          '';
          ExecStop = ''
            /bin/sh -c '${pkgs.docker-compose}/bin/docker-compose -f ${dockerfile} kill'
          '';
        };
      };

      virtualisation.docker.enable = mkDefault true;
      virtualisation.docker.enableOnBoot = mkDefault true;

      networking.firewall = let
        ports =
          []
          ++ (
            if cfg.openFirewall
            then [cfg.port]
            else []
          );
      in {
        allowedTCPPorts = ports;
      };
    };
  }
