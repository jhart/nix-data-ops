{
  lib,
  pkgs,
  config,
  ...
}:
with lib; let
  cfg = config.nix-data-ops.ballista;
in {
  options = {
    nix-data-ops.ballista = {
      enable = mkOption {
        default = false;
        description = "Enable Apache Ballista";
      };

      # Scheduler options
      scheduler = {
        enable = mkOption {
          default = false;
          description = "Starts the Apache Arrow Ballista scheduler.";
        };
        port = mkOption {
          default = 50050;
          description = "The port the scheduler listens to.";
        };
        external-host = mkOption {
          default = "localhost";
          description = "Host name or IP address so that executors can connect to this scheduler";
        };

        cluster-backend = mkOption {
          default = "sled";
          description = "The configuration backend for the scheduler cluster state, possible values: etcd, memory, sled";
          type = types.enum ["etcd" "memory" "sled"];
        };

        args = mkOption {
          default = {};
          type = types.attrsOf types.str;
          description = "Other command line arguments to pass to the scheduler";
          example = lib.literalMD "
          {
            log-dir = /tmp;
          }
          ";
        };

        openFirewall = mkOption {
          default = false;
          description = "Opens the firewall for the scheduler.";
        };
      };

      # Scheduler UI options
      scheduler-ui = {
        enable = mkOption {
          default = false;
          description = "Starts the Apache Arrow Ballista scheduler UI.";
        };

        port = mkOption {
          default = 3000;
          description = "The port the scheduler ui listens to";
        };

        openFirewall = mkOption {
          default = false;
          description = "Opens the firewall for the scheduler UI.";
        };
      };

      # Executors - provice as an attr set so that multiple instances can be started
      executors = mkOption {
        description = ''
          Apache Arrow Ballista Executor
        '';
        default = {};
        type = types.attrsOf (types.submodule {
          options = {
            enable = mkOption {
              default = false;
              description = "Enable executor";
            };
            port = mkOption {
              type = types.ints.u16;
              description = "The port";
            };
            scheduler-host = mkOption {
              default = "localhost";
              description = "The hostname of the scheduler.";
            };
            scheduler-port = mkOption {
              default = 50050;
              description = "The port of the scheduler.";
            };
            external-host = mkOption {
              default = "localhost";
              description = "Host name or IP address to register with scheduler so that other executors can connect to this executor. If none is provided, the scheduler will use the connecting IP address to communicate with the executor.";
            };
            args = mkOption {
              default = {};
              type = types.attrsOf types.str;
              description = ''Other command line arguments to pass to the executor.'';
              example = ''
                {
                  cache-dir = "/tmp";
                }
              '';
            };
            openFirewall = mkOption {
              default = false;
              description = "Opens the firewall for the executor.";
            };
          };
        });
      };
    };
  };

  config = let
    scheduler-args = toString (lib.attrsets.mapAttrsToList (name: value: "--${name} ${value}") cfg.scheduler.args);
  in
    mkIf cfg.enable {
      systemd.services =
        {
          # Scheduler UI config
          ballista-scheduler-ui = mkIf cfg.scheduler-ui.enable {
            wantedBy = ["multi-user.target"];
            description = "Ballista Scheduler UI";
            serviceConfig = {
              Type = "simple";
              WorkingDirectory = "${pkgs.ballista-ui}";
              ExecStart = ''
                /bin/sh -c 'DISABLE_ESLINT_PLUGIN=true PORT=${toString cfg.scheduler-ui.port} ${pkgs.yarn}/bin/yarn start'
              '';
              ExecStop = "/bin/kill -2 $MAINPID";
            };
          };

          # Scheduler config
          ballista-scheduler = mkIf cfg.scheduler.enable {
            wantedBy = ["multi-user.target" "ballista-executor-ui.service"];
            description = "Ballista Scheduler";
            serviceConfig = {
              Type = "simple";
              ExecStart = ''
                /bin/sh -c 'RUST_LOG=info ${pkgs.ballista}/bin/ballista-scheduler --bind-port ${toString cfg.scheduler.port} --external-host ${cfg.scheduler.external-host} --cluster-backend ${cfg.scheduler.cluster-backend} ${scheduler-args}'
              '';
              ExecStop = "/bin/kill -2 $MAINPID";
            };
          };
        }
        // (
          # Executors
          lib.mapAttrs' (name: cfg2:
            nameValuePair ("ballista-executor-" + name) (
              mkIf cfg2.enable {
                wantedBy = ["multi-user.target"];
                description = "Ballista Executor";
                requires = ["ballista-scheduler.service"];
                serviceConfig = {
                  Type = "simple";
                  ExecStart = "/bin/sh -c 'RUST_LOG=info ${pkgs.ballista}/bin/ballista-executor --bind-port ${toString cfg2.port} --scheduler-host ${cfg2.scheduler-host} --scheduler-port ${toString cfg2.scheduler-port} --external-host ${cfg2.external-host} ${toString (lib.attrsets.mapAttrsToList (name: value: "--${name} ${value}") cfg2.args)}'";
                  ExecStop = "/bin/kill -2 $MAINPID";
                };
              }
            ))
          cfg.executors
        );

      # Networking config
      networking.firewall = let
        ports =
          []
          ++ lib.attrsets.mapAttrsToList (name: value: value.port) (lib.attrsets.filterAttrs (name: value: value.openFirewall == true) cfg.executors)
          ++ (
            if cfg.scheduler.openFirewall
            then [cfg.scheduler.port]
            else []
          )
          ++ (
            if cfg.scheduler-ui.openFirewall
            then [cfg.scheduler-ui.port]
            else []
          );
      in {
        allowedTCPPorts = ports;
      };

      # Provide package
      environment.systemPackages = with pkgs; [
        ballista-ui
      ];
    };
}
