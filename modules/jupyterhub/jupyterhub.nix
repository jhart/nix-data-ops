{
  lib,
  pkgs,
  config,
  self,
}: let
  cfg = config.nix-data-ops.jupyterhub;
  pyarrow-hotfix = pkgs.python3Packages.buildPythonPackage rec {
    pname = "pyarrow_hotfix";
    version = "0.6";
    format = "pyproject";

    src = pkgs.python3Packages.fetchPypi rec {
      inherit pname version;
      sha256 = "sha256-edPgMPf/iQ1AihAKwW1vALFNRKUC14l82fw+OlNOmUU=";
    };

    nativeBuildInputs = [
      pkgs.python311Packages.hatchling
    ];

    pythonImportsCheck = [
      "pyarrow_hotfix"
    ];
  };

  env_python = pkgs.python3.withPackages (
    pythonPackages:
      with pkgs.python3Packages;
        [
          datafusion
          importlib-metadata
          ipykernel
          minio
          pandas
          polars
          py4j
          pyarrow
          pyarrow-hotfix
          pyspark
          pypdf
          pymupdf
          pdfminer
          scikit-learn
          pkgs.pyballista
          pkgs.deltalake
          pkgs.delta-spark
        ]
        ++ cfg.packages
  );
in
  with lib; {
    options.nix-data-ops.jupyterhub = {
      enable = mkOption {
        default = false;
        description = "Enable JupyterHub";
      };

      host = mkOption {
        default = "0.0.0.0";
        type = types.str;
        description = "JupyterHub host address";
      };

      port = mkOption {
        default = 9833;
        type = types.port;
        description = "JupyterHub port";
      };

      packages = mkOption {
        type = types.listOf types.package;
        default = with pkgs.python3Packages; [pandas];
        example = lib.literalMD "with pkgs.python3Packages; [ pandas ]";
      };

      openFirewall = mkOption {
        default = false;
        description = "Opens the firewall port";
        type = types.bool;
      };
    };

    config = mkIf cfg.enable {
      services.jupyterhub = {
        enable = true;
        port = cfg.port;
        host = cfg.host;
        kernels = {
          python3 = {
            displayName = "Python 3 for Spark";
            argv = [
              "${env_python.interpreter}"
              "-m"
              "ipykernel_launcher"
              "-f"
              "{connection_file}"
            ];
            language = "python";
            logo32 = "${env_python}/${env_python.sitePackages}/ipykernel/resources/logo-32x32.png";
            logo64 = "${env_python}/${env_python.sitePackages}/ipykernel/resources/logo-64x64.png";
          };
        };
        extraConfig = ''
          c.Spawner.env_keep = ['PATH', 'PYTHONPATH', 'CONDA_ROOT', 'CONDA_DEFAULT_ENV', 'VIRTUAL_ENV', 'LANG', 'LC_ALL', 'JUPYTERHUB_SINGLEUSER_APP', 'JAVA_HOME', 'SPARK_HOME' ]
          c.Spawner.environment = { "JAVA_HOME": "${pkgs.jdk}", "SPARK_HOME": "${pkgs.spark}"}
        '';
      };

      environment.variables = {
        PYSPARK_PYTHON = mkForce "${env}/bin/${env.executable}";
        PYSPARK_ALLOW_INSECURE_GATEWAY = "1";
        # PYTHONPATH = "$PYTHONPATH:${pkgs.python311Packages.py4j}:${env}:${pkgs.python311Packages.pyspark}:${pkgs.spark}/python:${pkgs.spark/python/lib/py4j-0.10.9.7-src.zip}";
        JAVA_HOME = "${pkgs.jdk}";
      };

      networking.firewall = let
        ports =
          []
          ++ (
            if cfg.openFirewall
            then [cfg.port]
            else []
          );
      in {
        allowedTCPPorts = ports;
      };
    };
  }
