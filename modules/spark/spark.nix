{
  lib,
  pkgs,
  config,
}: let
  cfg = config.nix-data-ops.spark;
in
  with lib; {
    options = {
      nix-data-ops.spark = {
        master = {
          enable = mkEnableOption "Spark master";
          bind = mkOption {
            default = "0.0.0.0";
            type = types.str;
            description = "Address the Apache Spark master service binds to";
          };
          port = mkOption {
            default = 7077;
            type = types.port;
            description = "Port for Apache Spark master service";
          };
          port_ui = mkOption {
            default = 7078;
            description = "Port for the Apache Spark web UI";
          };

          openFirewall = mkOption {
            default = false;
            type = types.bool;
            description = "Opens the http and web ui ports";
          };
        };

        worker = {
          enable = mkEnableOption "Spark worker";

          master = mkOption {
            default = "127.0.0.1:7077";
            type = types.str;
            description = "Address of the Apache Spark master";
          };
          port = mkOption {
            default = 7079;
            type = types.port;
            description = "Port for Apache Spark worker service";
          };
          port_ui = mkOption {
            default = 7080;
            description = "Port for the Apache Spark worker web UI";
          };
          max_memory = mkOption {
            default = "";
            type = types.str;
            example = lib.literalMD "4g";
            description = ''
              The total amount of memory the worker can use
            '';
          };
          max_cores = mkOption {
            default = "";
            type = types.str;
            example = lib.literalMD "4";
            description = ''
              The total number of CPU cores the worker can use
            '';
          };

          openFirewall = mkOption {
            default = false;
            type = types.bool;
            description = "Opens the http and web ui ports";
          };
        };
      };
    };
    config = mkMerge [
      (mkIf config.nix-data-ops.spark.master.enable {
        services.spark.master = {
          enable = true;
          bind = cfg.master.bind;
          extraEnvironment = {
            SPARK_MASTER_PORT = toString cfg.master.port;
            SPARK_MASTER_WEBUI_PORT = toString cfg.master.port_ui;
          };
        };

        environment.variables = {
          JAVA_HOME = "${pkgs.jdk}";
          SPARK_HOME = "${pkgs.spark}";
          SPARK_DIST_CLASSPATH = "";
          PYSPARK_PYTHON = mkDefault "${pkgs.python3Packages.python}/bin/${pkgs.python3Packages.python.executable}";
          PYTHONPATH = mkDefault "$PYTHONPATH:$PYTHONPATH:${pkgs.python311Packages.py4j}";
          SPARKR_R_SHELL = "${pkgs.R}/bin/R";
        };

        environment.systemPackages = with pkgs; [
          spark
          jdk
          python311Packages.py4j
          python311
          scala_2_12
          R
        ];

        networking.firewall = let
          ports =
            []
            ++ (
              if cfg.master.openFirewall
              then [cfg.master.port cfg.master.port_ui]
              else []
            );
        in {
          allowedTCPPorts = ports;
        };
      })
      (mkIf cfg.worker.enable {
        services.spark.worker = {
          enable = true;
          master = cfg.worker.master;
          extraEnvironment = {
            # SPARK_WORKER_PORT = toString cfg.worker.port;
            # SPARK_WORKER_WEBUI_PORT = toString cfg.worker.port_ui;
            SPARK_WORKER_MEMORY = cfg.worker.max_memory;
            SPARK_WORKER_CORES = cfg.worker.max_cores;
          };
        };

        environment.variables = {
          JAVA_HOME = "${pkgs.jdk}";
          SPARK_HOME = "${pkgs.spark}";
          SPARK_DIST_CLASSPATH = "";
          PYSPARK_PYTHON = mkDefault "${pkgs.python3Packages.python}/bin/${pkgs.python3Packages.python.executable}";
          PYTHONPATH = mkDefault "$PYTHONPATH:$PYTHONPATH:${pkgs.python311Packages.py4j}";
          SPARKR_R_SHELL = "${pkgs.R}/bin/R";
        };

        networking.firewall = let
          ports =
            []
            ++ (
              if cfg.worker.openFirewall
              then [cfg.worker.port cfg.worker.port_ui]
              else []
            );
        in {
          allowedTCPPorts = ports;
        };
      })
    ];
  }
