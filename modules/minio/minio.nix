{
  lib,
  pkgs,
  config,
}: let
  cfg = config.nix-data-ops.minio;
in
  with lib; {
    options = {
      nix-data-ops.minio = {
        enable = mkEnableOption "Minio FS";

        host = mkOption {
          default = "localhost";
          type = types.str;
          description = "Host address of the server";
          example = lib.literalMD "0.0.0.0";
        };

        port = mkOption {
          default = 9000;
          type = types.port;
          description = "Port of the server";
        };

        port_ui = mkOption {
          default = 9001;
          type = types.port;
          description = "Port of the web UI";
        };

        region = mkOption {
          default = "us-east-1";
          type = types.str;
          description = "The physical location of the server. By default it is set to us-east-1, which is same as AWS S3’s and Minio’s default region.";
        };

        rootCredentialsFile = mkOption {
          type = types.nullOr types.path;
          default = null;
          description = lib.mdDoc ''
            File containing the MINIO_ROOT_USER, default is "minioadmin", and
            MINIO_ROOT_PASSWORD (length >= 8), default is "minioadmin"; in the format of
            an EnvironmentFile=, as described by systemd.exec(5).
          '';
          example = lib.literalMD "
            pkgs.writeTextFile {
              name = " minio-credentials ";
              text = ''
                MINIO_ROOT_USER=minio
                MINIO_ROOT_PASSWORD=miniorootpwd
              '';
            };
          ";
        };

        dataDir = mkOption {
          default = ["/var/lib/minio/data"];
          type = types.listOf (types.either types.path types.str);
          description = lib.mdDoc "The list of data directories or nodes for storing the objects. Use one path for regular operation and the minimum of 4 endpoints for Erasure Code mode.";
        };

        extraEnvironment = mkOption {
          type = types.attrsOf types.str;
          description = lib.mdDoc "Extra environment variables to pass to Minio. See https://min.io/docs/minio/kubernetes/upstream/reference/operator-environment-variables.html";
          default = {};
          example = {
            MINIO_OPERATOR_RUNTIME = "EKS";
          };
        };

        package = mkOption {
          default = pkgs.minio;
          defaultText = literalExpression "pkgs.minio";
          type = types.package;
          description = lib.mdDoc "Minio package to use.";
        };

        openFirewall = mkOption {
          default = false;
          type = types.bool;
          description = "Opens the http and web ui ports";
        };
      };
    };

    config = mkIf cfg.enable {
      services.minio = {
        enable = true;
        dataDir = cfg.dataDir;
        rootCredentialsFile = cfg.rootCredentialsFile;
        region = cfg.region;
        listenAddress = "${cfg.host}:${toString cfg.port}";
        consoleAddress = "${cfg.host}:${toString cfg.port_ui}";
        package = cfg.package;
      };

      environment.systemPackages = with pkgs; [
        minio-client
      ];

      environment.sessionVariables =
        cfg.extraEnvironment
        // {
          MINIO_API_SELECT_PARQUET = "on";
        };

      networking.firewall = let
        ports =
          []
          ++ (
            if cfg.openFirewall
            then [cfg.port cfg.port_ui]
            else []
          );
      in {
        allowedTCPPorts = ports;
      };
    };
  }
