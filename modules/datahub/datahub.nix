{
  lib,
  pkgs,
  config,
}: let
  cfg = config.nix-data-ops.datahub;
  docker_path = toString ./docker;
in
  with lib; {
    options.nix-data-ops.datahub = {
      enable = mkOption {
        default = false;
        description = "Enable Apache datahub";
      };

      port = mkOption {
        default = 5810;
        type = types.port;
        description = "Port of the web interface";
      };

      openFirewall = mkOption {
        default = false;
        type = types.bool;
        description = "Opens the firewall for Apache datahub";
      };
    };
    config = mkIf cfg.enable {
      systemd.services.apache-datahub = {
        wantedBy = ["multi-user.target"];
        after = ["docker.service" "docker.socket"];
        environment = {
          DATAHUB_ELASTIC_PORT="5300";
          DATAHUB_GMS_PORT="5301";
          DATAHUB_KAFKA_BROKER_PORT="5302";
          DATAHUB_MAPPED_FRONTEND_PORT=toString cfg.port;
          DATAHUB_MAPPED_GMS_PORT="5303";
          DATAHUB_MAPPED_KAFKA_BROKER_PORT="5304";
          DATAHUB_MAPPED_NEO4J_BOLT_PORT="5405";
          DATAHUB_MAPPED_NEO4J_HTTP_PORT="5306";
          DATAHUB_MAPPED_SCHEMA_REGISTRY_PORT="5307";
          DATAHUB_MAPPED_ZK_PORT="5308";
          DATAHUB_NEO4J_HTTP_PORT="5309";
          DATAHUB_SCHEMA_REGISTRY_PORT="5310";
          DATAHUB_ZK_PORT="5311";
          HOME="/var/lib/datahub";
        };

        serviceConfig = {
          Type = "simple";
          ExecStart = ''
            /bin/sh -c '${pkgs.docker-compose}/bin/docker-compose -f ${docker_path}/docker-compose.override.yml -f ${docker_path}/docker-compose.yml up'
          '';
          ExecStop = ''
            /bin/sh -c '${pkgs.docker-compose}/bin/docker-compose -f ${docker_path}/docker-compose.override.yml -f ${docker_path}/docker-compose.yml kill'
          '';
        };
      };

      virtualisation.docker.enable = mkDefault true;
      virtualisation.docker.enableOnBoot = mkDefault true;

      networking.firewall = let
        ports =
          []
          ++ (
            if cfg.openFirewall
            then [cfg.port]
            else []
          );
      in {
        allowedTCPPorts = ports;
      };
    };
  }
