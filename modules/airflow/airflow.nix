# Module for Apache Airflow
# When enabled, it starts the Apache Airflow docker container using the official docker-compose.yaml file
{
  lib,
  pkgs,
  config,
}: let
  cfg = config.nix-data-ops.airflow;
  dockerfile = builtins.toFile "airflow-docker" (builtins.replaceStrings ["8080:8080"] ["${toString cfg.port}:8080"] (lib.readFile ./docker-compose.yaml));
in
  with lib; {
    options.nix-data-ops.airflow = {
      enable = mkOption {
        default = false;
        description = "Enable Apache Airflow orchestration";
      };

      dataDir = mkOption {
        default = /tmp;
        type = types.path;
        description = "The directory where Airflow stores its data";
      };

      uid = mkOption {
        default = 1000;
        description = "UID of the user";
      };

      port = mkOption {
        default = 8080;
        type = types.port;
        description = "Port of the web interface";
      };

      openFirewall = mkOption {
        default = false;
        type = types.bool;
        description = "Opens the firewall for Apache Airflow";
      };
    };
    config = mkIf cfg.enable {
      systemd.services.apache-airflow = {
        wantedBy = ["multi-user.target"];
        after = ["docker.service" "docker.socket"];
        environment = {
          AIRFLOW_UID = toString cfg.uid;
          AIRFLOW_PROJ_DIR = "${toString cfg.dataDir}";
        };

        serviceConfig = {
          Type = "simple";
          WorkingDirectory = cfg.dataDir;
          ExecStart = ''
            /bin/sh -c '${pkgs.docker-compose}/bin/docker-compose -f ${dockerfile} up'
          '';
          ExecStop = ''
            /bin/sh -c '${pkgs.docker-compose}/bin/docker-compose -f ${dockerfile} kill'
          '';
        };
      };

      virtualisation.docker.enable = mkDefault true;
      virtualisation.docker.enableOnBoot = mkDefault true;

      networking.firewall = let
        ports =
          []
          ++ (
            if cfg.openFirewall
            then [cfg.port]
            else []
          );
      in {
        allowedTCPPorts = ports;
      };
    };
  }
