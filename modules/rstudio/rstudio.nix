{
  lib,
  pkgs,
  config,
  ...
}:
with lib; let
  cfg = config.nix-data-ops.rstudio-server;
in {
  options.nix-data-ops.rstudio-server = {
    enable = mkOption {
      default = false;
      description = "Enable RStudio server";
    };

    # RStudio server run on port 8787
    host = mkOption {
      default = "localhost";
      type = types.str;
      description = "Host address of the server";
      example = lib.literalMD "127.0.0.1";
    };

    r-packages = mkOption {
      type = types.listOf types.package;
      default = with pkgs.rPackages; [dplyr tidymodels tidyverse ggplot2];
      example = lib.literalMD "[ pkgs.rPackages.dplyr ]";
    };

    openFirewall = mkOption {
      default = false;
      type = types.bool;
      description = "Opens the http port";
    };
  };

  config = let
    my-rstudio = pkgs.rstudioServerWrapper.override {
      packages = cfg.r-packages;
    };
  in
    mkIf cfg.enable {
      services.rstudio-server = {
        enable = true;
        package = my-rstudio;
        listenAddr = cfg.host;
        rserverExtraConfig = "
        ";
      };

      environment.systemPackages = [
        my-rstudio
      ];

      networking.firewall = let
        ports =
          []
          ++ (
            if cfg.openFirewall
            then [8787]
            else []
          );
      in {
        allowedTCPPorts = ports;
      };
    };
}
