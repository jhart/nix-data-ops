{
  description = "Data Science Module";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    nixos-generators,
    rust-overlay,
    ...
  }: let
    supportedSystems = ["x86_64-linux" "aarch64-linux"];
    lib = nixpkgs.lib;
    forEachSupportedSystem = f:
      nixpkgs.lib.genAttrs supportedSystems (system:
        f {
          pkgs = import nixpkgs {
            inherit system;
            overlays = [
              self.overlays.default
              rust-overlay.overlays.default
            ];
          };
          system = system;
        });
  in
    with lib; rec {
      formatter = forEachSupportedSystem ({
        pkgs,
        system,
      }:
        pkgs.alejandra);

      # ----------------------------
      # Packages -------------------
      # ----------------------------
      packages = forEachSupportedSystem ({
        pkgs,
        system,
      }: rec {
        ballista-ui = import ./packages/ballista-ui/default.nix {inherit pkgs lib;};
        ballista = import ./packages/ballista/default.nix {inherit pkgs lib;};
        pyballista = import ./packages/pyballista/default.nix {inherit pkgs lib;};
        deltalake = import ./packages/deltalake/default.nix {inherit pkgs lib;};
        delta-spark = import ./packages/delta-spark/default.nix {inherit pkgs lib;};
        # Reexports
        spark = pkgs.spark;
        minio = pkgs.minio;
        minio-client = pkgs.minio-client;

        # ----------------------------
        # Generated Virtual Machines
        # ----------------------------
        vm-do-spark-master = nixos-generators.nixosGenerate {
          system = "x86_64-linux";
          modules = import ./systems/spark-master.nix {inherit self pkgs;};
          format = "do";
        };
        vm-do-spark-worker = nixos-generators.nixosGenerate {
          system = "x86_64-linux";
          modules = import ./systems/spark-worker.nix {inherit self pkgs;};
          format = "do";
        };
      });

      # ----------------------------
      # Overlays -------------------
      # ----------------------------
      overlays.default = import ./overlays/default.nix;

      # ----------------------------
      # Module definitions ---------
      # ------------------------ ----
      nixosModules.nix-data-ops = let
        system = "x86_64-linux";
      in
        {
          config,
          pkgs,
          ...
        }: let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [
              self.overlays.default
              rust-overlay.overlays.default
            ];
          };
        in {
          options.nix-data-ops = {
            spark = (import ./modules/spark/spark.nix {inherit lib pkgs config;}).options.nix-data-ops.spark;
            superset = (import ./modules/superset/superset.nix {inherit lib pkgs config;}).options.nix-data-ops.superset;
            minio = (import ./modules/minio/minio.nix {inherit lib pkgs config;}).options.nix-data-ops.minio;
            airflow = (import ./modules/airflow/airflow.nix {inherit lib pkgs config;}).options.nix-data-ops.airflow;
            jupyterhub = (import ./modules/jupyterhub/jupyterhub.nix {inherit self lib pkgs config;}).options.nix-data-ops.jupyterhub;
            rstudio-server = (import ./modules/rstudio/rstudio.nix {inherit lib pkgs config;}).options.nix-data-ops.rstudio-server;
            ballista = (import ./modules/ballista/ballista.nix {inherit lib pkgs config;}).options.nix-data-ops.ballista;
            datahub = (import ./modules/datahub/datahub.nix {inherit lib pkgs config;}).options.nix-data-ops.datahub;
          };
          config = mkMerge [
            (import ./modules/spark/spark.nix {inherit lib pkgs config;}).config
            (import ./modules/superset/superset.nix {inherit lib pkgs config;}).config
            (import ./modules/minio/minio.nix {inherit lib pkgs config;}).config
            (import ./modules/airflow/airflow.nix {inherit lib pkgs config;}).config
            (import ./modules/jupyterhub/jupyterhub.nix {inherit self lib pkgs config;}).config
            (import ./modules/rstudio/rstudio.nix {inherit lib pkgs config;}).config
            (import ./modules/ballista/ballista.nix {inherit lib pkgs config;}).config
            (import ./modules/datahub/datahub.nix {inherit lib pkgs config;}).config
          ];
        };

      # ----------------------------
      # System configurations ------
      # ----------------------------
      nixosConfigurations = let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        };
        system = "x86_64-linux";
      in {
        spark-master = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/spark-master.nix {inherit self pkgs;};
        };
        spark-worker = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/spark-worker.nix {inherit self pkgs;};
        };
        datahub = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/datahub.nix { inherit self pkgs;};
        };
        miniofs = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/miniofs.nix {inherit self pkgs;};
        };
        jupyterhub = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/jupyterhub.nix {inherit self pkgs;};
        };
        rstudio-server = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/rstudio-server.nix {inherit self pkgs;};
        };
        ballista-scheduler = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/ballista-scheduler.nix {inherit self pkgs;};
        };
        airflow = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/airflow.nix {inherit self pkgs;};
        };
        full-stack = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = import ./systems/full-stack.nix {inherit self pkgs;};
        };
      };
    };
}
