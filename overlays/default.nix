final: prev: {
  ballista = prev.callPackage ../packages/ballista/default.nix {};
  ballista-ui = prev.callPackage ../packages/ballista-ui/default.nix {};
  pyballista = prev.callPackage ../packages/pyballista/default.nix {};
  deltalake = prev.callPackage ../packages/deltalake/default.nix {};
  delta-spark = prev.callPackage ../packages/delta-spark/default.nix {};
}
