# Nix Data Ops
This flake aims to provide the tools to quickly spin up a data science dev environment locally or in the cloud.

It provides
- packages
- nixos modules
- pre-configured system configurations
- VMs that can be used in the cloud

Many of the packages and module options have been taken straight from Nixpkgs, so credit belongs to all those who contributed there.
This flake also adds some packages, but it mainly reorganizes and tweaks the existing Nixpkgs stuff to suit my needs and I hope others may find this useful.

# Status
This project is very much work in progress and should not be used in production.

# Howto use
The flake provides different assets that can be used: packages, nixos modules, system configurations, and VM's

To use the flake in your own Nix project and get access to packages, modules and configurations, include it into your flake, e.g.
```nix
{
  description = "Data Science Module";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    nix-data-ops = {
      url = "gitlab:jhart/nix-data-ops";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  output = {nixpkgs, nix-data-ops, ...}: {
    nixosConfigurations = {
      "my-nixos" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";

        modules = [
          nix-data-ops.nixosModules.nix-data-ops
          ...
        ];
        ...
      };
    };
    ...
  };
}
````

## Packages
- Spark (via nixpkgs)
- MinioFS (via nixpkgs)
- Ballista
- deltalake Python package
- pyballista Python package
- delta-spark Python package

## Modules
- Spark master
- Spark workers
- MinioFS (with Parquet support)
- Apache Airflow
- Apache Ballista
- JupytherHub
- RStudio

- ToDo: 
  - Amundsen (Data Management)
  
## System configurations
The flake comes with several pre-configured system configurations that can be build as a VM and deployed to a server.

To build a system, e.g. the Spark Master system, run
```bash
  # When inside the cloned repo
  sudo nixos-rebuild build-vm --flake .#spark-master

  # Otherwise, on NixOS
  sudo nixos-rebuild build-vm --flake gitlab:jhart/nix-data-ops#spark-master
```

## VMs
The following Virtual Machine Images are included

- Digitalocean:
  - Spark Master
  - Spark Worker

To build an image, run
```bash
nix build .#vm-do-spark-master
````
The resulting image can be found in the ```./results``` folder and upload it to your cloud provider.


# Example: How to use Delta tables with MinioFS and Spark in a Jupyter Notebook
An example Jupyter notebook can be found in ```examples/Spark.ipynb```

```python
from pyspark.sql import SparkSession
from delta import *
from minio import Minio

import os
os.environ["JAVA_HOME"] = "/nix/store/nr8j20n4ldgar0npki50513a56h2zhfc-openjdk-21+35/"
os.environ["SPARK_HOME"] = "/nix/store/7qynb0qs9kxijydk87vcf4n9lniwzrj6-spark-3.5.1"
! echo $JAVA_HOME
! echo $SPARK_HOME

spark = (
    SparkSession
    .builder.master("spark://10.65.65.1:4040")
    .appName("Test")
    .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
    .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
)

spark = configure_spark_with_delta_pip(spark, extra_packages=["org.apache.hadoop:hadoop-aws:3.3.4"]).getOrCreate()

# Configure Spark to work with MinioFS
sc = spark.sparkContext
sc._jsc.hadoopConfiguration().set("fs.s3a.access.key", "Vy59TqRbM7laxZqiH55A")
sc._jsc.hadoopConfiguration().set("fs.s3a.secret.key", "oORMDjpXFGhF1hzP4orTVsUPZEe35lQZ7evkAO3a")
sc._jsc.hadoopConfiguration().set("fs.s3a.endpoint", "http://localhost:9011")
sc._jsc.hadoopConfiguration().set("fs.s3a.path.style.access", "true")
sc._jsc.hadoopConfiguration().set("fs.s3a.connection.ssl.enabled", "false")
sc._jsc.hadoopConfiguration().set("fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
sc._jsc.hadoopConfiguration().set("fs.s3a.connection.ssl.enabled", "false")

# Create Minio connection
client = Minio(
    "10.65.65.1:9011", 
    "Vy59TqRbM7laxZqiH55A", 
    "oORMDjpXFGhF1hzP4orTVsUPZEe35lQZ7evkAO3a",
    secure=False
)

buckets = client.list_buckets()
for bucket in buckets:
    print(bucket.name, bucket.creation_date)

# Get bucket
bucket_name = "deltanew2"

found = client.bucket_exists(bucket_name)
if not found:
    client.make_bucket(bucket_name)
    print("Created bucket", bucket_name)
else:
    print("Bucket", bucket_name, "already exists")

# Create a dataframe
dfs = spark.createDataFrame(
    [
        ("a", 1),
        ("b", 2),
        ("c", 3),
        ("d", 4),
        ("d", 5),
    ],
    schema=["foo", "bar"],
)

dfs.show()

# Write dataframe to MinioFS in Delta format
dfs.write.format("delta").mode("overwrite").save(f"s3a://{bucket_name}/coffee-delta")

# Read delta table from MinioFS
df = spark.read.format("delta").load(f"s3a://{bucket_name}/coffee-delta")
df.show()

```


# Credit
Many of the packages and module options have been taken directly from Nixpkgs, thus I am deeply grateful to everybody who contributed to the existing data science stack in Nixpkgs.
