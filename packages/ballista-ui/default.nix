{
  pkgs,
  lib,
}: let
  repo = fetchGit {
    url = "https://github.com/apache/arrow-ballista";
    rev = "a8ee11e55cfae4b7418f7044580318d33be9669e";
  };
in
  pkgs.mkYarnPackage rec {
    pname = "ballista-ui";
    version = "0.12.0";

    src = "${repo}/ballista/scheduler/ui";

    packageJSON = src + "/package.json";

    offlineCache = pkgs.fetchYarnDeps {
      yarnLock = "${src}/yarn.lock";
      hash = "sha256-8DqZz9g+RV8bmigtm1Czmh6qWKWS32auZb/KrxyH5TQ=";
    };

    installPhase = ''
      runHook preInstall
      mkdir $out
      cp -R deps/scheduler-ui/* $out
      rm $out/node_modules -Rf
      mkdir $out/node_modules
      cp -R $node_modules $out
      runHook postInstall
    '';

    doDist = false;

    meta = with lib; {
      description = "The apache ballista ui (dev version)";
      platforms = with platforms; linux ++ darwin;
      license = licenses.mit;
    };
  }
