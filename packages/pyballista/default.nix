{
  lib,
  pkgs,
  ...
}: let
  project_name = "pyballista";
  project_version = "0.12.0";

  repo = fetchGit {
    url = "https://github.com/apache/arrow-ballista.git";
    rev = "321af95db65a522ae2e618295e1caa77cedf8c13";
  };
  wheel_tail = "cp38-abi3-linux_x86_64";

  wheel = pkgs.rustPlatform.buildRustPackage rec {
    pname = "ballista";
    version = "0.12.0";

    src = repo;

    cargoHash = "sha256-/glrPWSsMXtnfC6NNzL7DgWrc9AKDnpYUFIoDXUUoWw=";

    cargoLock = {
      lockFile = ./Cargo.lock;
      outputHashes = {
        "datafusion-python-35.0.0" = "sha256-p59QAEEMEai4iO+fhd9yaWnGC3sKMj9RThqeSQPEmNg=";
      };
    };

    nativeBuildInputs = [
      pkgs.protobuf
      pkgs.maturin
    ];

    doCheck = false;

    postPatch = ''
      cd python
      ln -s ${./Cargo.lock} Cargo.lock
    '';

    buildPhase = ''
      maturin build --target-dir ./target
    '';

    installPhase = ''
      mkdir -p $out
      cp target/wheels/${project_name}-${project_version}-${wheel_tail}.whl $out/
    '';
  };
in
  pkgs.python3Packages.buildPythonPackage rec {
    pname = "pyballista";
    version = "0.12.0";
    format = "wheel";

    src = "${wheel}/${project_name}-${project_version}-${wheel_tail}.whl";

    doCheck = false;

    pythonImportChecks = [project_name];

    meta = {
      description = "";
      homepage = "https://github.com/apache/arrow-ballista";
      license = lib.licenses.unlicense;
      maintainers = ["jhartma"];
    };
  }
