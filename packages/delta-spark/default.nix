{
  lib,
  pkgs,
}: let
in
  pkgs.python3Packages.buildPythonPackage rec {
    pname = "delta-spark";
    version = "3.1.0";
    format = "wheel";

    src = pkgs.fetchurl {
      url = "https://files.pythonhosted.org/packages/0a/ee/dcf357307bdf8ef5436bb147159cf42c339e556b09c2eb1aec7e7b472022/delta_spark-3.1.0-py3-none-any.whl";
      sha256 = "sha256-o1zJC0VvAv+tnFlg1slhzqFSbZj7CSj2mJMFVyaW/3I=";
    };

    doCheck = false;

    pythonImportChecks = [
      "delta"
    ];

    meta = {
      description = "";
      homepage = "https://github.com/delta-io/delta";
      license = lib.licenses.unlicense;
      maintainers = ["jhartma"];
    };
  }
