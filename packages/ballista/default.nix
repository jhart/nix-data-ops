{
  lib,
  pkgs,
}:
pkgs.rustPlatform.buildRustPackage rec {
  pname = "ballista";
  version = "0.12.0";

  src = pkgs.fetchFromGitHub {
    owner = "apache";
    repo = pname;
    rev = version;
    hash = "sha256-5mAU9EuQ4sIBLW3DAdPAYc0BEqL/pevaQwjVE0sfEfw=";
  };

  cargoHash = "sha256-/glrPWSsMXtnfC6NNzL7DgWrc9AKDnpYUFIoDXUUoWw=";

  cargoLock = {
    lockFile = ./Cargo.lock;
  };

  nativeBuildInputs = [
    pkgs.protobuf
    pkgs.jdk
  ];

  JAVA_HOME = "${pkgs.jdk}";
  LIBCLANG_PATH = "${pkgs.llvmPackages.libclang.lib}/lib";

  doCheck = false;

  postPatch = ''
    ln -s ${./Cargo.lock} Cargo.lock
  '';

  buildFeatures = [
    "ballista-executor"
    "ballista-scheduler"
    "s3"
  ];

  meta = {
    description = "";
    homepage = "https://github.com/apache/arrow-ballista";
    license = lib.licenses.unlicense;
    maintainers = ["jhartma"];
  };
}
