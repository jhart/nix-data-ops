{
  lib,
  pkgs,
  ...
}: let
  project_name = "deltalake";
  project_version = "0.17.2";

  repo = fetchGit {
    url = "https://github.com/delta-io/delta-rs.git";
    rev = "6a7c684d9b76e1dc4851096912fbb2ebf3c6a806";
  };

  wheel_tail = "cp38-abi3-linux_x86_64";

  wheel = pkgs.rustPlatform.buildRustPackage rec {
    pname = "deltalake";
    version = "0.17.2";

    src = repo;

    cargoHash = "sha256-/glrPWSsMXtnfC6NNzL7DgWrc9AKDnpYUFIoDXUUoWw=";

    OPENSSL_NO_VENDOR = 1;

    cargoLock = {
      lockFile = ./Cargo.lock;
    };

    nativeBuildInputs = [
      pkgs.protobuf
      pkgs.maturin
      pkgs.pkg-config
    ];

    buildInputs = [
      pkgs.openssl
      pkgs.openssl.dev
    ];

    doCheck = false;

    postPatch = ''
      cd python
      ln -s ${./Cargo.lock} Cargo.lock
    '';

    buildPhase = ''
      maturin build --target-dir ./target --out wheels --release
    '';

    installPhase = ''
      mkdir -p $out
      cp wheels/${project_name}-${project_version}-${wheel_tail}.whl $out/
    '';
  };
in
  pkgs.python3Packages.buildPythonPackage rec {
    pname = "deltalake";
    version = "0.17.2";
    format = "wheel";

    src = "${wheel}/${project_name}-${project_version}-${wheel_tail}.whl";

    doCheck = false;

    pythonImportChecks = [project_name];

    meta = {
      description = "";
      homepage = "https://github.com/delta-io/delta-rs/tree/main/python/";
      license = lib.licenses.unlicense;
      maintainers = ["jhartma"];
    };
  }
